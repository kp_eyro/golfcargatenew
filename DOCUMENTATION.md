# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Cara menggunakan ##
* __Mengirimkan jumlah penumpang pada mesin antrian, untuk keperluan mencetak tiket antrian__

### Request Code ###

``` 
 curl -X POST \
 http://192.168.1.134:2210/golfCar/checkIn \
 -H 'Content-Type: application/json' \
 -H 'cache-control: no-cache' \
 -d '{
 "machineCode": "DA1",
 "numberOfPassenger": 2
 }' 
```


__Catatan:__ 

`machineCode` _diperoleh dari setting mesin, sedangkan_ `numberOfPassenger` _diperoleh dari input user_


### Response From Server ###

```
{
   "sequenceNumber":"DA100001", 
   "totalPassenger":0, 
   "standInLine":0, 
   "numberOfPassenger":2, 
   "active":true, 
   "uploaded":false, 
   "uniqueCode":"DA1154658250600001" 
}
```


__Catatan:__

`sequenceNumber` _nomer yang dicetak pada layar dan juga tiket, sedangkan_ `standInLine` _adalah 
jumlah antrian penumpang yang dicetak pada layar dan tiket_


* __Pada perangkat android, petugas gate antrian mengirimkan data__

### Request Code ###

```
curl -X POST \
   http://192.168.1.134:2210/golfCar/checkOut \
   -H 'Content-Type: application/json' \
   -H 'cache-control: no-cache' \
   -d '{"uniqueCode":"DA1154658250600001"}'
```


__Catatan:__

`uniqueCode` _diperoleh dari hasil scan barcode yang ada pada tiket penumpang_


### Response From Server ###

`{"sequenceNumber":"DA100001", "numberOfPassenger":2}`


__Catatan:__

`sequenceNumber` _nomer yang dicetak pada layar perangkat android, sedangkan_ `numberOfPassenger` _adalah 
jumlah penumpang yang dicetak pada layar perangkat android_



* __Pada perangkat android, petugas gate melakukan login__

_Service belum dibuat_



* __Pada perangkat android, petugas gate memasukkan sequnceNumber secara manual__

_Service belum dibuat_ 