package com.example.rizky.golfcarpickupgate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Objects;

public class SuccessActivity extends AppCompatActivity {

    TextView jTvPassNumber, jTvNumPass;
    String inputCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        jTvPassNumber = findViewById(R.id.tvPassNumber);
        jTvNumPass = findViewById(R.id.tvNumPass);

        Bundle bundle = getIntent().getExtras();
        jTvPassNumber.setText(bundle.getString("sequenceNumber"));
        jTvNumPass.setText(bundle.getString("numberOfPassenger"));

        //jTvPassNumber.setText(getIntent().getStringExtra("test"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        startActivity(new Intent(this, MainActivity.class));
    }
}
