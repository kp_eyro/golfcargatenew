package com.example.rizky.golfcarpickupgate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button jBtnSignIn;
    EditText jEtEmail, jEtPassword;
    String email, password;
    String url = "http://192.168.0.101:2210/golfCar/login";
    //String url = "http://192.168.1.134:2210/golfCar/login";
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        jEtEmail = findViewById(R.id.etEmail);
        jEtPassword = findViewById(R.id.etPassword);
        jBtnSignIn = findViewById(R.id.btnSignIn);

        jBtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //jEtEmail.setText("mohammad.badrullami@gmail.com");
                //jEtPassword.setText("admin1234");
                email = jEtEmail.getText().toString();
                password = jEtPassword.getText().toString();

                if (validateLogin(email, password)) {
                    doLogin(email, password);
                }
            }
        });
    }

    private boolean validateLogin(String emailCheck, String passCheck) {
        if (emailCheck == null || emailCheck.trim().length() == 0) {
            Toast.makeText(this, "Email is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (passCheck == null || passCheck.trim().length() == 0) {
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void doLogin(final String email, final String password) {
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        JSONObject params = new JSONObject();
        try {
            params.put("email", email);
            params.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JsonObjectRequest jsonObjectRequestLogin = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String access_token = response.getString("access_token");
                            String username = response.getString("username");
                            if (access_token != null && username.equalsIgnoreCase(email)) {
                                sharedPreferences.edit().putBoolean("logged", true).apply();
                                sharedPreferences.edit().putString("access_token", access_token).apply();

                                Toast.makeText(getApplicationContext(), "Welcome " + username,
                                        Toast.LENGTH_SHORT).show();
                                Intent IntentSignIn = new Intent(LoginActivity.this, MainActivity.class);
                                IntentSignIn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                IntentSignIn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(IntentSignIn);
                            } else {
                                throw new JSONException("Wrong input");
                            }
                        } catch (JSONException e) {
                            //e.printStackTrace();
                            try {
                                String message = response.getString("message");
                                Toast.makeText(getBaseContext(), "Error: " + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error getting response!",
                        Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequestLogin);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
