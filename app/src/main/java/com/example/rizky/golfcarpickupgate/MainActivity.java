package com.example.rizky.golfcarpickupgate;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    BarcodeDetector barcodeDetector;
    Button jBtnSubmitCode, jBtnLogout;
    CameraSource cameraSource;
    EditText jEtInputCode;
    String seqCode, finalSeqCode;
    String url = "http://192.168.0.101:2210/golfCar/checkOut";
    //String url = "http://192.168.1.134:2210/golfCar/checkOut";
    SurfaceView jCamPreview;
    TextView jTvCode;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    final int RequestCameraPermissionID = 1001;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(jCamPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jBtnSubmitCode = findViewById(R.id.btnSubmitCode);
        jBtnLogout = findViewById(R.id.btnLogout);
        jCamPreview = findViewById(R.id.camPreview);
        jEtInputCode = findViewById(R.id.etInputCode);
        jTvCode = findViewById(R.id.tvCode);

        createCameraSource();

        jBtnSubmitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seqCode = jEtInputCode.getText().toString();
                //finalSeqCode = "DA100" + seqCode;
                requestAPIsequence(seqCode);
            }
        });

        jBtnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
                editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
    }

    private void createCameraSource() {
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.CODE_128 | Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(640, 640)
                .build();

        jCamPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA}, RequestCameraPermissionID);
                        return;
                    }
                    cameraSource.start(jCamPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() > 0) {
                    //Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
                    //vibrator.vibrate(100);
                    //jTvCode.setText(barcodes.valueAt(0).displayValue);
                    seqCode = barcodes.valueAt(0).displayValue;
                    requestAPIunique(seqCode);
                }
            }
        });
    }

    private void requestAPIsequence(final String seqCode) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JSONObject params = new JSONObject();
        try {
            params.put("sequenceNumber", seqCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JsonObjectRequest jsonObjectRequestSequence = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String sequenceNumber = response.getString("sequenceNumber");
                            String numberOfPassenger = response.getString("numberOfPassenger");
                            if ((sequenceNumber != null) && (numberOfPassenger != null)) {
                                Toast.makeText(getBaseContext(), "Enjoy your ride!", Toast.LENGTH_SHORT).show();
                                Bundle bundle = new Bundle();
                                bundle.putString("sequenceNumber", sequenceNumber);
                                bundle.putString("numberOfPassenger", numberOfPassenger);
                                Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            } else {
                                throw new JSONException("Ticket not valid");
                            }
                        } catch (JSONException e) {
                            //e.printStackTrace();
                            try {
                                String alert = response.getString("alert");
                                Toast.makeText(getBaseContext(), "Alert: " + alert, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error getting response!",
                        Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequestSequence);
    }

    private void requestAPIunique(final String uniqueCode) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JSONObject params = new JSONObject();
        try {
            params.put("uniqueCode", uniqueCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JsonObjectRequest jsonObjectRequestUnique = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String sequenceNumber = response.getString("sequenceNumber");
                            String numberOfPassenger = response.getString("numberOfPassenger");
                            if ((sequenceNumber != null) && (numberOfPassenger != null)) {
                                Toast.makeText(getBaseContext(), "Enjoy your ride!", Toast.LENGTH_SHORT).show();
                                Bundle bundle = new Bundle();
                                bundle.putString("sequenceNumber", sequenceNumber);
                                bundle.putString("numberOfPassenger", numberOfPassenger);
                                Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            } else {
                                throw new JSONException("Ticket not valid");
                            }
                        } catch (JSONException e) {
                            //e.printStackTrace();
                            try {
                                String alert = response.getString("alert");
                                Toast.makeText(getBaseContext(), "Alert: " + alert, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error getting response!",
                        Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequestUnique);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
